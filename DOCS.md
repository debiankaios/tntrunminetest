# tntrun docs

# Table of Contents
* [1. Create an arena](#1-create-an-arena)
* [2. Add music](#2-add-music)


## 1. Create an arena

1. Type in Chat:
```
/tntrun create <your arena name> <min. players for arena> <max. players for arena>
```

2. Type in Chat
```
/tntrun edit <your arena name>
```

3. Go to spawners and set up were you can spawn. You must set max. Players spawner.
4. Set signs
5. Go to BGM and choose your Back Ground Music. There is Crusade by Kevin MacLeod which you can choose. Warning: This music is on the cc-by-sa license please leave credits.
6. Go to settings and got to arena propeties. Here you can chose were the TNT spawn. It's between tnt_arena_pos_1 and tnt_arena_pos_2. You can have multiple positions to define multiple areas. The position list should be a list of positions. Example: { { x=x,y=y,z=z } , { x=x,y=y,z=z } , { x=x,y=y,z=z } } 
7. Save the settings
8. Now you can play

## 2. Add music

1. Go to:
"~" means the userhome.
Linux: ~/.minetest/mods/tntrun/music  
Windows: WC:\\Minetest/mods/tntrun/music or C:\\Games\Minetest/mods/tntrun/music
Mac: ~/Library/Application Support/minetest/
2. Add your music in .ogg
3. Now follow the 5. Step in create a arena
