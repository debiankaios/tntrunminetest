local S = minetest.get_translator("tntrun")

local function send_message(arena,num_str)
    arena_lib.HUD_send_msg_all("title", arena, num_str, 1,nil,0xFF0000)
    --arena_lib.HUD_send_msg_all(HUD_type, arena, msg, <duration>, <sound>, <color>)
end


arena_lib.on_load("tntrun", function(arena)

  -- repair the arena

    if arena.tnt_area_pos_1[x] and arena.tnt_area_pos_1[y] and arena.tnt_area_pos_1[z] and arena.tnt_area_pos_2[x] and arena.tnt_area_pos_2[y] and arena.tnt_area_pos_2 [z] then
        arena.tnt_area_pos_1 = { arena.tnt_area_pos_1 }
        arena.tnt_area_pos_2 = { arena.tnt_area_pos_2 }
    end
  end

    for k = 1, #arena.tnt_area_pos_1 do

        local pos1 = arena.tnt_area_pos_1[k]
        local pos2 = arena.tnt_area_pos_2[k]
        local x1 = pos1.x
        local x2 = pos2.x
        local y1 = pos1.y
        local y2 = pos2.y
        local z1 = pos1.z
        local z2 = pos2.z
        if x1 > x2 then
            local temp = x2
            x2 = x1
            x1 = temp
        end
        if y1 > y2 then
            local temp = y2
            y2 = y1
            y1 = temp
        end
        if z1 > z2 then
            local temp = z2
            z2 = z1
            z1 = temp
        end

        for x = x1,x2 do
            for y = y1,y2 do
                for z = z1,z2 do
                    minetest.set_node({x=x,y=y,z=z}, {name="tntrun:tnt"})
                end
            end
        end

    end

  for pl_name, stats in pairs(arena.players) do
    local message = S('How it work:')
    minetest.chat_send_player(pl_name,message)
    message = minetest.colorize('#f47e1b', S('Punch with torches TNT to fight with other players'))
    minetest.chat_send_player(pl_name,message)
    message = minetest.colorize('#e6482e', S('But warning, the TNT on which you standing fall down'))
    minetest.chat_send_player(pl_name,message)
    arena.players[pl_name].lives = arena.lives

  end
  send_message(arena,'3')
    minetest.after(1, function(arena)

        send_message(arena,'2')
        minetest.after(1, function(arena)
            send_message(arena,'1')
            minetest.after(1, function(arena)
                arena_lib.HUD_send_msg_all("title", arena, "Run!", 1,nil,0xE6482E)

                local item = ItemStack("tntrun:torch")


                for pl_name, stats in pairs(arena.players) do
                    sumo.invincible[pl_name] = false
                    local player = minetest.get_player_by_name(pl_name)
                    player:get_inventory():set_stack("main", 1, item)


                end



            end, arena)

        end, arena)

    end, arena)
end)

-- arena_lib.on_start('tntrun', function(arena)
--     --give players the torch at the start
--     for pl_name, stats in pairs(arena.players) do
--         local player = minetest.get_player_by_name(pl_name)
--         local inv = player:get_inventory()
--         local stack = ItemStack("tntrun:torch")
--         inv:set_stack("main", 1, stack)
--     end
-- end)

arena_lib.on_death('tntrun', function(arena, p_name, reason)
    arena_lib.remove_player_from_arena(p_name, 1)
end)

minetest.register_on_joinplayer(function(player)
	local inv = player:get_inventory()
	local stack = ItemStack("tntrun:torch")
	local taken = inv:remove_item("main", stack)
end)

if minetest.get_modpath("aes_xp") then
  arena_lib.on_celebration('tntrun', function(arena, winner_name)
    aes_xp.add_xp(winner_name, 25)
    minetest.chat_send_player(winner_name, "You got 25 XP for winning tntrun")
  end)
end
