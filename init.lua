local S = minetest.get_translator("tntrun")
tntrun = {}
dofile(minetest.get_modpath("tntrun") .. "/settings.lua")

local player_jump = tntrun.player_jump
local player_speed = tntrun.player_speed


local player_jump = tntrun.player_jump
local player_speed = tntrun.player_speed


arena_lib.register_minigame("tntrun", {
  name = "Tnt Run!",
  icon = "tntrun_icon",
  celebration_time = 3,
  load_time = 3,
  prefix = "[Sp] ",
  celebration_time = 3,
  load_time = 4,
  queue_waiting_time = 20,
  show_minimap = true,
  properties = {
    tnt_area_pos_1 = {{x = "<xpos>", y = "<ypos>", z = "<zpos>"}},
    tnt_area_pos_2 = {{x = "<xpos>", y = "<ypos>", z = "<zpos>"}},
  },

  disabled_damage_types = {"punch"},
  hotbar = {
    slots = 1,
    background_image = "tntrun_gui_hotbar.png"
  },
  in_game_physics = {
    speed = tntrun.player_speed,
    jump = tntrun.player_jump,
  },


})

minetest.register_privilege("tntrun_admin", S("Needed for Tntrun"))

if not minetest.get_modpath("lib_chatcmdbuilder") then
    dofile(minetest.get_modpath("tntrun") .. "/chatcmdbuilder.lua")
end
dofile(minetest.get_modpath("tntrun") .. "/nodes.lua")
dofile(minetest.get_modpath("tntrun") .. "/auto.lua")
dofile(minetest.get_modpath("tntrun") .. "/tnt.lua")

minetest.register_privilege("tntrun_admin", S("Needed for Tntrun admin setup"))

ChatCmdBuilder.new("tntrun", function(cmd) -- In music is a music Cusade by KevinMacLeod which should using for one map

  -- create arena
  cmd:sub("create :arena", function(name, arena_name)
      arena_lib.create_arena(name, "tntrun", arena_name)
  end)

  cmd:sub("create :arena :minplayers:int :maxplayers:int", function(name, arena_name, min_players, max_players)
      arena_lib.create_arena(name, "tntrun", arena_name, min_players, max_players)
  end)

  -- remove arena
  cmd:sub("remove :arena", function(name, arena_name)
      arena_lib.remove_arena(name, "tntrun", arena_name)
  end)

  -- list of the arenas
  cmd:sub("list", function(name)
      arena_lib.print_arenas(name, "tntrun")
  end)

  -- enter editor mode
  cmd:sub("edit :arena", function(sender, arena)
      arena_lib.enter_editor(sender, "tntrun", arena)
  end)

  -- enable and disable arenas
  cmd:sub("enable :arena", function(name, arena)
      arena_lib.enable_arena(name, "tntrun", arena)
  end)

  cmd:sub("disable :arena", function(name, arena)
      arena_lib.disable_arena(name, "tntrun", arena)
  end)

end, {
  description = [[

    (/help tntrun)

    Use this to configure your arena:
    - create <arena name> [min players] [max players]
    - edit <arena name>
    - enable <arena name>

    Other commands:
    - remove <arena name>
    - disable <arena>
    ]],
    privs = {
        tntrun_admin = true
    },
})



arena_lib.on_enable("tntrun", function(arena, p_name)
  if #arena.tnt_area_pos_1 ~= #arena.tnt_area_pos_2 or arena.tnt_area_pos_1 [1] == { x = "<xpos>" , y = "<ypos>", z = "<zpos>" } or arena.tnt_area_pos_2 [1] == { x = "<xpos>", y = "<ypos>", z = "<zpos>"} then
    minetest.chat_send_player(p_name,"Missing params in the positions")
    return false
  end
  return true
end)


